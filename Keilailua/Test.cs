﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keilailua
{
    [TestFixture]
    class Test
    {
        [Test]
        // voidaanko luokka luoda
        public void ekatesti ()
        {
            Peli testipeli = new Peli();

        }

        [Test]
        // tallentuuko kaadettujen keilojen lukumäärä
        public void tokatesti()
        {
            Peli testipeli = new Peli();
            testipeli.heitto(0, 5);
            int Heitontulos = testipeli.lueheitto(0);
            Assert.AreEqual(5, Heitontulos);
        }
        [Test]
        // laske vuoron tulos
        public void kolmastesti()
        {
            Peli testipeli = new Peli();
            testipeli.heitto(2, 8);
            testipeli.heitto(3, 1);
            int Vuorontulos = testipeli.vuorontulos(1);
            Assert.AreEqual(9, Vuorontulos);

        }

        [Test]
        // laske kokonaistulos
        public void neljastesti()
        {
            Peli testipeli = new Peli();
            for (int i = 0; i <= 20; i++)
            {
                testipeli.heitto(i, 1);
            }

            int kokonaistulos = 0;

            for (int i = 0; i <= 9; i++)
            {
                kokonaistulos = kokonaistulos + testipeli.vuorontulos(i);
            }
            Assert.AreEqual(21, kokonaistulos);
        }


    }
}